@extends($activeTemplate.'layouts.frontend')

@section('content')
    <section class="pt-90 pb-90">
        <div class="container">
            {{--            <div class="row justify-content-center margin--top-adjustment">--}}

            {{--            </div>--}}
            <div class="container group row">
                @foreach($packages as $package)
                    <div class="col-md-4">
                    <div class="grid-1-5 m-3">
                        <h2>{{$package->name}}</h2>
                        <h1 class="">{{$package->price}} %</h1>
                       <hr>
                        <p>{{$package->description}}</p>
                    </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection


@push('style')
    <style>
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
        }
        .group:after {
            content: "";
            display: table;
            clear: both;
        }

        .grid-1-5 {
            border: 2px solid #5d4e65;
            min-height: 400px;
            padding: 1.25em;
            position: relative;
            text-align: center;
            border-radius: 15px;
            transition: all 0.2s ease-in-out;
        }


        .grid-1-5:hover {
            background-color: #53455b;
            *zoom: 1;
            filter: progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr='#FF53455B', endColorstr='#FF201D22');
            background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzUzNDU1YiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzIwMWQyMiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
            background-size: 100%;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #b9f87f), color-stop(100%, #efef25));
            background-image: -moz-linear-gradient(top, #b9f87f 0%, #efef25 100%);
            background-image: -webkit-linear-gradient(top, #b9f87f 0%, #efef25 100%);
            background-image: linear-gradient(to bottom, #b9f87f 0%, #efef25 100%);
            border-top: 2px solid #ec7a37;
            border-bottom: 2px solid #ff4f69;
            box-shadow: 0px 0px 10px 0px #fad945;
            transform: scale(1.025);
            z-index: 2;
        }

        .grid-1-5:hover:before, .grid-1-5:hover:after {
            content: "";
            position: absolute;
            background-color: #f67d35;
            *zoom: 1;
            filter: progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr='#FFF67D35', endColorstr='#FFFF4F68');
            background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2Y2N2QzNSIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2ZmNGY2OCIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
            background-size: 100%;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #f67d35), color-stop(100%, #ff4f68));
            background-image: -moz-linear-gradient(top, #f67d35 0%, #ff4f68 100%);
            background-image: -webkit-linear-gradient(top, #f67d35 0%, #ff4f68 100%);
            background-image: linear-gradient(to bottom, #f67d35 0%, #ff4f68 100%);
            top: -2px;
            bottom: -2px;
            width: 2px;
        }

        .grid-1-5:hover:before {
            left: -2px;
        }

        .grid-1-5:hover:after {
            right: -2px;
        }

        .grid-1-5:hover .button {
            background-color: #ee7a36;
            *zoom: 1;
            filter: progid:DXImageTransform.Microsoft.gradient(gradientType=1, startColorstr='#FFEE7A36', endColorstr='#FFEB495D');
            background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuMCIgeTE9IjAuNSIgeDI9IjEuMCIgeTI9IjAuNSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2VlN2EzNiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2ViNDk1ZCIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
            background-size: 100%;
            background-image: -webkit-gradient(linear, 0% 50%, 100% 50%, color-stop(0%, #ee7a36), color-stop(100%, #eb495d));
            background-image: -moz-linear-gradient(left, #ee7a36 0%, #eb495d 100%);
            background-image: -webkit-linear-gradient(left, #ee7a36 0%, #eb495d 100%);
            background-image: linear-gradient(to right, #ee7a36 0%, #eb495d 100%);
        }

        h2,
        h3,
        p,
        ul {
            margin: 0;
        }

        h2 {
            font-size: 1em;
            font-weight: 400;
            margin: 0 0 0.5em;
        }

        h3 {
            font-size: 1.5em;
            letter-spacing: 0.0625em;
            margin: 0 0 0.3333333333333333em;
        }

        p {
            font-size: 0.875em;
        }

        p,
        ul {
            margin: 0 0 1.5em;
        }

        ul {
            color: #796583;
            font-size: 0.75em;
            list-style-type: none;
            padding: 0;
        }

        ul li {
            margin: 0 0 0.8333333333333333em;
        }

        .button {
            background-color: #9c83aa;
            border-radius: 20px;
            color: #fff;
            font-size: 1em;
            font-weight: 700;
            padding: 0.75em 1.5em;
            position: absolute;
            bottom: 1.25em;
            left: 50%;
            margin-left: -60px;
            text-decoration: none;
            width: 120px;
        }

        .uppercase,
        .button,
        h2 {
            text-transform: uppercase;
        }

        sup,
        .small {
            font-size: 0.6125em;
        }

        {{--        --}}
        .action-form .form-group + .form-group {
            margin-top: 0px;
        }

        .image-prev {
            height: 200px;
            width: 200px;
            border-radius: 50%;
            overflow: hidden;
            border: 1px solid #b9b9b9;
            margin: 0 auto;
            margin-top: -150px;
            margin-bottom: 20px;
        }

        .margin--top-adjustment {
            margin-top: 100px;
        }

        .custom-file-input {
            color: transparent;
            width: 100px;
            opacity: 1 !important;
            height: auto;

        }

        .custom-file-input::-webkit-file-upload-button {
            visibility: hidden;
        }

        .custom-file-input::before {
            content: 'Upload File';
            display: inline-block;
            background: #198754 !important;
            border: 1px solid #999;
            border-radius: 3px;
            padding: 9px 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
            font-weight: 700;
            font-size: 10pt;

        }


        .custom-file-input:active {
            outline: 0;
        }

        .custom-file-input:active::before {
            background: #198754;
        }

    </style>
@endpush

@push('script')
    <script>
        $(function () {
            "use strict";
            @if ($mobile_code)
            $(`option[data-code={{ $mobile_code }}]`).attr('selected', '');
            @endif
            $('select[name=country]').change(function () {
                $('input[name=mobile_code]').val($('select[name=country] :selected').data('mobile_code'));
                $('input[name=country_code]').val($('select[name=country] :selected').data('code'));
                $('.mobile-code').text('+' + $('select[name=country] :selected').data('mobile_code'));
            }).change();

            function showImagePreview(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#prev').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function removeImage() {
                var _empty = '';
                $('#prev').attr('src', "{{ route('placeholderImage', '200x200') }}");
                $('.custom-file-input').val(_empty);
            }

            $(".custom-file-input").change(function () {
                showImagePreview(this);
            });

            $('.remove').on('click', function () {
                removeImage();
            })
        })

    </script>
@endpush
