<!-- volunteer section start -->
<section class="pt-150 pb-150">\
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="section-header text-center">
                    <h2 class="section-title">Partner Organizations</h2>
                    <p>We envision transformed a new world. We try to restore through donation,
                        revoke the poverty, make a new era.</p>
                </div>
            </div>
        </div>
        <div class="row mb-none-30">


                <div class="col-xl-4 col-sm-6 mb-30 wow fadeInUp" data-wow-duration="0.3s" data-wow-delay="0.3s">
                    <div class="volunteer-card hover--effect-1">
                        <div class="volunteer-card__thumb">
                            <img style="height: 200px" src="{{asset('assets/templates/basic/images/rotary.jpg')}}" alt="image"
                                 class="w-100">
                        </div>
                        <div class="volunteer-card__content">
                            <h4 class="name">Rotary Club</h4>
{{--                            <span class="designation">sdfsdf</span>--}}
                        </div>
                    </div><!-- volunteer-card end -->
                </div>

                <div class="col-xl-4 col-sm-6 mb-30 wow fadeInUp" data-wow-duration="0.3s" data-wow-delay="0.3s">
                    <div class="volunteer-card hover--effect-1">
                        <div class="volunteer-card__thumb">
                            <img  style="height: 200px" src="{{asset('assets/templates/basic/images/nawaloka_hospita.jpg')}}" alt="image"
                                 class="w-100">
                        </div>
                        <div class="volunteer-card__content">
                            <h4 class="name">Nawaloka Hospital</h4>
{{--                            <span class="designation">sdfsdf</span>--}}
                        </div>
                    </div><!-- volunteer-card end -->
                </div>

                <div class="col-xl-4 col-sm-6 mb-30 wow fadeInUp" data-wow-duration="0.3s" data-wow-delay="0.3s">
                    <div class="volunteer-card hover--effect-1">
                        <div class="volunteer-card__thumb">
                            <img style="height: 200px" src="{{asset('assets/templates/basic/images/australian-red.png')}}" alt="image"
                                 class="w-100">
                        </div>
                        <div class="volunteer-card__content">
                            <h4 class="name">Red Cross Sri Lanka</h4>
{{--                            <span class="designation">sdfsdf</span>--}}
                        </div>
                    </div><!-- volunteer-card end -->
                </div>


        </div>
    </div>
</section>
<!-- volunteer section end -->
