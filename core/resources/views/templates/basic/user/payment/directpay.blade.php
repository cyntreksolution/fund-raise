@extends($activeTemplate.'layouts.frontend')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card card-deposit shadow">
                    <div class="card-header">
                        <h5 class="card-title">@lang('DirectPay Payment')</h5>
                    </div>
                    <div class="card-body card-body-deposit">


                        <div class="card-wrapper"></div>
                        <br><br>

                        <div class="form-group row" id="div_transfer">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div id="card_container">
                                    <input type="hidden" name="amount" value="" id="amount">
                                    <input type="hidden" name="merchantId" value="{{ env('APP_IMERCHANT') }}" id="merchantId">
                                    <input type="hidden" name="invoice" value="" id="invoice">
                                    <input type="hidden" name="merchantId" value="" id="merchantId">
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('script')
    @if (env('APP_ENV')=='local')
        <script src="{{$data->demo_payment}}"></script>
    @else
        <script src="{{$data->live_payment}}"></script>
    @endif

    <script>
        @if (env('APP_ENV')=='local')
        const merchantId = '{{$data->demo_merchantId}}';
        @else
        const merchantId = '{{$data->live_merchantId}}';
         @endif
        const API_Key = '{{$data->api_key}}';
        const amount = '{{$data->amount}}';

        DirectPayCardPayment.init({
            container: 'card_container',
            merchantId: merchantId,
            amount: amount,
            refCode: '{{$data->ref_code}}',
            currency: '{{$data->currency}}',
            type: 'ONE_TIME_PAYMENT',
            customerEmail: '{{$data->customer_email}}',
            customerMobile: '{{$data->customer_phone}}',
            description: 'test products',
            debug: true,
            responseCallback: responseCallback,
            errorCallback: errorCallback,
            logo: 'https://test.com/directpay_logo.png',
            apiKey: API_Key
        });

        //response callback.
        function responseCallback(result) {
            var data = {result: result};

            $.ajax({
                type: "POST",
                url: '{{route('ipn.directpay')}}',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if (result.success) {
                        Notifications.showSuccessMsg(result.message);
                    } else {
                        Notifications.showErrorMsg(result.message);
                    }
                }
            });
            // console.log("successCallback-Client", result);
            // alert(JSON.stringify(result));
        }

        //error callback
        function errorCallback(result) {
            var data = {result: result};
            $.ajax({
                type: "POST",
                url: '{{route('ipn.directpay')}}',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if (result.success) {
                        Notifications.showErrorMsg(result.message);
                    } else {
                        Notifications.showErrorMsg(result.message);
                    }
                    location.reload();
                }
            });
            // console.log("successCallback-Client", result);
            alert(JSON.stringify(result));
        }
    </script>

@endpush


@push('style')
    <style>
        .card-deposit{
            margin: 60px;
        }
        .card-header{
            background: mediumseagreen;
        }
        .card-header h5{
            color: #ffffff;
        }

        .btn-success{
            background: mediumseagreen;
            margin:5px 0;
        }

        .btn-success:hover{

            background: mediumseagreen;
            border:1px solid mediumseagreen;
        }
    </style>
@endpush
