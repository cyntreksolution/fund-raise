@extends('admin.layouts.app')
@section('panel')

    <div class="row">
        <div class="col-lg-12">

            <div class="card">
                <div class="card-body p-0">

                    <div class="table-responsive--md table-responsive">
                        <table class="table table--light style--two" id="myTable">
                            <thead>
                            <tr>

                                <th scope="col">@lang('Name')</th>
                                <th scope="col">@lang('Price')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            @forelse($package as $key => $cat)
                                <tr class="filt">

                                    <td data-label="@lang('Name')">{{$cat->name}}</td>
                                    <td data-label="@lang('Price')">{{$cat->price}}</td>

                                    <td data-label="@lang('Action')">

                                        <a href="javascript:void(0)" class="icon-btn editmodal" title=""
                                           data-original-title="Edit" data-id="{{$cat->id}}" data-name = "{{$cat->name}}" data-description = "{{$cat->description}}" data-price = "{{$cat->price}}"
                                           data-action =
                                        "{{route('admin.package.update',$cat->id)}}" >
                                        <i class="las la-edit text--shadow"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-muted text-center">@lang($empty_mesasage)</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>


                    </div>
                </div>

                @if($package->hasPages())
                    <div class="card-footer py-4">
                        {{ $package->links('admin.partials.paginate') }}
                    </div>
                @endif
            </div><!-- card end -->
        </div>
    </div>




    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('Add Package')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">@lang('Name:')</label>
                                    <input type="text" name="name" class="form-control" placeholder="Package Name"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="">@lang('Price:')</label>
                                    <input type="text" name="price" class="form-control" placeholder="Price"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="">@lang('Description:')</label>
                                    <input type="text" name="description" class="form-control" placeholder="Description"
                                           required>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="form-control btn btn--primary" value="Add Package">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('Close')</button>
                </div>
            </div>
        </div>
    </div>


    {{-- editCategory Modal --}}




    <!-- Modal -->
    <div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('Update Category')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input name="slug" class="btn btn-primary" type="hidden">
                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="">@lang('Name'):</label>
                                        <input type="text" name="name" class="form-control" aria-describedby="helpId">
                                    </div>

                                    <div class="form-group">
                                        <label for="">@lang('Price'):</label>
                                        <input type="text" name="price" class="form-control" aria-describedby="helpId">
                                    </div>
                                    <div class="form-group">
                                        <label for="">@lang('Description:')</label>
                                        <input type="text" name="description" class="form-control" placeholder="Description"
                                               required>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <input type="submit" class="form-control btn btn--primary" value="Update">
                            </div>

                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('Close')</button>
                </div>
            </div>
        </div>
    </div>



@endsection

@push('style')
    <style>

        .image-upload .thumb .profilePicPreview {
            width: 100%;
            height: 192px;
            display: block;
            border: 3px solid #f1f1f1;
            box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.25);
            border-radius: 10px;
            background-size: cover !important;
            background-position: top;
            background-repeat: no-repeat;
            position: relative;
            overflow: hidden;
        }
    </style>
@endpush

@push('breadcrumb-plugins')

    <div class="d-flex flex-row-reverse">
        <div class="ml-5 mt-1">
            <a href="javascript:void(0)" class="btn btn-sm btn--primary box--shadow1 text--small" id="addCategory"><i
                        class="fa fa-fw fa-plus"></i> @lang('Add Package') </a>
        </div>

        <div>
            <div class="input-group has_append">
                <input type="text" name="search" class="form-control" placeholder="@lang('Search Package')" value=""
                       id="myInput">
                <div class="input-group-append">
                    <span class="bg--primary px-3"><i class="fa fa-search mt-2"></i></span>
                </div>
            </div>

        </div>


    </div>
@endpush



@push('script')

    <script>
        'use strict';

        $('#addCategory').on('click', function () {
            $('#modelId').modal('show');
        })


        $('.editmodal').on('click', function () {
            var modal = $('#editCategory');

            modal.find("input[name=slug]").val($(this).data('slug'));

            modal.find("input[name=name]").val($(this).data('name'));
            modal.find("input[name=price]").val($(this).data('price'));
            modal.find("input[name=description]").val($(this).data('description'));

            console.log($(this).data('description'))
            modal.find("form").attr('action', $(this).data('action'));

            modal.modal('show');

        })
    </script>

@endpush
