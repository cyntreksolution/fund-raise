<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }

    public function index()
    {
        $package = Package::latest()->paginate(10);
        $page_title = 'Package Page';
        $empty_mesasage = 'No Package Has been Created Yet';

        return view('admin.package.package', compact('page_title', 'empty_mesasage', 'package'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:50|unique:packages,id',
        ]);


        Package::create([
            'name' => $request->name,
            'price' => $request->price,
            'slug' => Str::slug($request->name),
            'description' => $request->description,
        ]);


        $notify[] = ['success', 'Package Created Successfully'];
        return back()->withNotify($notify);
    }

    public function updateCategory(Package $slug, Request $request)
    {
        $this->validate($request, [
            'name' => 'sometimes|required|max:50',
        ]);


        $slug->name = $request->name;
        $slug->price = $request->price;
        $slug->description = $request->description;
        $slug->slug = Str::slug($request->name);
        $slug->save();

        $notify[] = ['success', 'Updated Successfull'];
        return back()->withNotify($notify);

    }

    public function front(){
        $packages = Package::all();
        $page_title = "Chargers";
        $info = json_decode(json_encode(getIpInfo()), true);
        $mobile_code = @implode(',', $info['code']);
        return view($this->activeTemplate.'chargers',compact('packages','page_title','mobile_code'));
    }

}
