<?php

namespace App\Http\Controllers\Gateway\directpay;

use App\Deposit;
use App\GeneralSetting;
use App\Http\Controllers\Gateway\PaymentController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Token;
use Illuminate\Support\Facades\Session;


class ProcessController extends Controller
{

    /*
     * StripeJS Gateway
     */
    public static function process($deposit)
    {
        $general = GeneralSetting::first();
        $flutterAcc = json_decode($deposit->gateway_currency()->gateway_parameter);

        $alias = $deposit->gateway->alias;

        $send['amount'] = number_format(round($deposit->final_amo,2),2, '.','');
        $send['ref_code'] = $deposit->trx;
        $send['currency'] = $deposit->method_currency;
        $send['demo_merchantId'] = $flutterAcc->demo_merchantId;
        $send['live_merchantId'] = $flutterAcc->live_merchantId;
        $send['demo_payment'] = $flutterAcc->demo_payment;
        $send['live_payment'] = $flutterAcc->live_payment;
        $send['api_key'] = $flutterAcc->api_key;
        $send['customer_email'] = Auth::user()->email ?? json_decode($general->anonymous)->email ;
        $send['customer_phone'] = Auth::user()->mobile ?? "123456789";
        $send['view'] = 'user.payment.'.$alias;
        $send['method'] = 'post';
        $send['url'] = route('ipn.'.$alias);
        return json_encode($send);
    }

    /*
     * StripeJS js ipn
     */
    public function ipn(Request $request)
    {
        $data_res = $request->result['data'];
        if (isset($data_res['status']) && $data_res['status'] == 'SUCCESS' && $data_res['description'] == 'Approved') {

            $data = Deposit::where('trx', $data_res['reference'])->orderBy('id', 'DESC')->first();

            if (number_format($data_res['amount'], 0) == number_format($data->final_amo, 0) && $data_res['reference'] == $data->trx && $data_res['currency'] == $data->method_currency && $data_res['status'] == 'SUCCESS' && $data_res['description'] == 'Approved' && $data->status == 0) {
                PaymentController::userDataUpdate($data->trx);
                $notify[] = ['success', 'Transaction was successful, Ref: ' . $data->trx];
            }else{
                $notify[] = ['error', 'Unable to Process'];
            }
        }else{
            $track = Session::get('Track');
            $data = Deposit::where('trx', $track)->orderBy('id', 'DESC')->first();
            $notify[] = ['error', 'Unable to Process'];
        }
        return redirect()->route(gatewayRedirectUrl(), ['slug' => @$data->campaign->slug, 'id' => @$data->campaign->id])->withNotify($notify);

    }
}
